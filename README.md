# Electric Potential Lab

In this lab we will be measuring electric potentials and plotting them with Python. There are 3 sheets and we will systematically fill our data tables for sheet-1,2, & 3.

## Lab Procedure

1. Set the power supply to 10V
2. Connect to leads of the power supply to the center of the long side of the sheet at the top and bottom.
3. Connect one side of the multimeter to one of the leads.
4. Change directories into your electric potential repository by using cd
5. Install the python dependencies by typing `pip install -r requirements.txt`
5. With the other lead, measure the electric potential for each position on the sheet inputting the data in sheet-1.py
6. When you want to see your plot, run:
```python3 plot.py sheet-1```
7. Once you've completed your plot, download it.
8. Repeat for the second sheet. This time placing one of the leads of the power supply and one of the leads of the multimeter on one of the metallic rails. Someone will need to hold them in place while you take data. Don't worry if you remove them at some point.  You can always put them back.
9. This time rather than running the command in Step 5, run the following command:

``` sh
/bin/ls sheet-2.py | entr -r python3 plot.py sheet-2
```
9. When you are running this command save your sheet every once and a while and see what happens!
10. Once you are finished, again download your .png
11. Repeat Steps 7 - 10 for the final sheet.

## Command Line Tools
1. Let's compare the difference between the install code and the python code we are running. From your lab directory type `cat ../hello-world/install.bash` This will output that file to the terminal. You can scroll up to see more of it. Now try `cat plot.py` In your Notebook write what you think the differences are between these (*HINT:* Look at the top of the python file, and remember we used a `pip install`)
2. Now that we have all of the data that we want, we want to combine all of our .png files into one pdf file. Do a websearch in order to find the correct command in order to do this.
3. Use your package manager to download the program if needed (`sudo apt install PROGRAM` on Windows and `brew install PROGRAM` on MacOS)
4. There are 3 ways to learn about the command. First let's try `PROGRAM -h` to output its help to ther terminal. This is the fastest way to see all of the options for the command.
5. Check out the manual page for your command using `man PROGRAM` man is paginated, meaning you can scroll the page with arrow keys. Once you've learned more about the command you can exit the man page by pressing `q` This is the most extensive reference for the command.
6. Now let's try a different command to learn about the command. Try `tldr PROGRAM`. This is the best way to get examples of the usage for the command. 

Between these three commands, you almost never need to do a websearch to learn about them. Try to use them on some of our other commands like `git`, `brew`, `ls`, `cd` and `apt`.

6. Once you've merged all of your images into a pdf, insert it as an image into your OneNote Notebook for the lab. You may have to open the PDF in your webbrowser, and print it. You'll have an option on Windows to print to OneNote. I'm not sure if this works on MacOS. If that's the case, simply insert it as an attachment

## Discussion

1. Now that you have your image in the Class Notebook. Describe the meaning of curves remembering our discussion of contour maps and the beautiful partially complete volcano I made. 
2. Write the equation for how you go from the Electric Potential to the Electric Field. For each plot, sketch the direction of the electric field lines.
3. Finally, ask any questions about the programming tools we used and I'll try to answer them on Friday.

