#!/usr/bin/env python3
import matplotlib
import numpy
import plotly.graph_objects as go
import sys

plot_data = __import__(sys.argv[1], fromlist=["main"])
fig = go.Figure(data = go.Contour(z=plot_data.my_data))

fig.show()


