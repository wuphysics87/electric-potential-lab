#!/usr/bin/env bash

if (( $# != 1 )); then
    printf "ERROR! You need the correct number of command line arguments!\n"
    printf "Try: bash rerun.bash ARG\n"

else
    filename=$1
    filesub=${filename:0:6}
    if [[ "$filesub" != "sheet-"  ]]; then
        printf "ERROR! wrong filename\n"
        printf "Try: bash rerun.bash sheet-#.py\n"
    else
        /bin/ls "*.py" | entr -r python plot.py "$filename"
    fi
fi
